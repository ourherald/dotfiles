#!/bin/sh
# Profile to refer to bashrc


if [ $(basename $SHELL) == $(basename $(command -v bash)) ]; then
	if [ -f $HOME/.bashrc ]; then
		. $HOME/.bashrc
	fi
elif [ $(basename $SHELL) == $(basename $(command -v zsh)) ]; then
    if [ -f $HOME/.zshrc ]; then
        . $HOME/.zshrc
    fi
fi

eval $(ssh-agent)

# Set custom paths
pathdef


test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"

