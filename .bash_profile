#!/usr/bin/env bash
PATH=$PATH:/usr/local/bin:$HOME/bin:$HOME/scripts
export PATH

if [[ $OSTYPE == linux-gnu ]]; then
	# Set KDE askpass program for ssh
	export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR"/ssh-agent.socket
fi

source $HOME/.bashrc
