# Put your custom themes in this folder.
# Example:

#PROMPT="%{$fg[red]%}%n%{$reset_color%}@%{$fg[blue]%}%m %{$fg[yellow]%}%~ %{$reset_color%}%% "
PROMPT="%{%(#~$fg[red]~$fg[cyan])%}%n%{$reset_color%}%{$fg[cyan]%}@%m %{$fg[yellow]%}%~ %{$reset_color%}%(!.#.$) "
#PROMPT="%{$fg[cyan]%}%n%{$reset_color%}%{$fg[cyan]%}@%m %{$fg[yellow]%}%~ %{$reset_color%}%(!.#.$) "
#PS1="%~ %{%(#~$fg[red]~$fg[blue])%}%#%{$fg[default]%} "

# This is what I had setup in bash
#if [[ "$color_prompt" == yes ]]; then
#    # Standard colors
#    atcolor='\[\e[36m\]'
#    promptcolor='\[\e[36m\]'
#    sepcolor=''
#    dircolor='\[\e[92m\]'
#    # Standard symbols
#        ubit='\u'
#        abit='@'
#        hbit='\h'
#        dbit='\W'
#        ebit='\$'
#
#    # Determine if machine is local or remote
#    if [[ -n "$SSH_CLIENT" ]] || [[ -n "$SSH_TTY" ]]; then
#                hostcolor='\[\e[33m\]'
#        else
#                hostcolor='\[\e[36m\]'
#        fi
#
#    printf '%s' "$cwd"
#
#        # Set user color and prompt (whether user is root, tim, or other)
#        if [[ $UID == 0 ]]; then
#                usercolor='\[\e[91m\]'
#        export PS1="$usercolor$ubit\[\e[m\]$atcolor$abit\[\e[m\]$hostcolor$hbit\[\e[m\]$sepcolor:\[\e[m\][$dircolor\$(short_pwd)\[\e[m\]] $promptcolor$ebit\[\e[m\] "
#        elif [[ $USER == tim ]]; then
#                usercolor='\[\e[36m\]'
#        export PS1="$usercolor$ubit\[\e[m\]$atcolor$abit\[\e[m\]$hostcolor$hbit\[\e[m\]$sepcolor:\[\e[m\][$dircolor\$(short_pwd)\[\e[m\]] $promptcolor$ebit\[\e[m\] "
#        else
#                usercolor='\[\033[38;5;11m\]'
#        export PS1="$usercolor$ubit\[\e[m\]$atcolor$abit\[\e[m\]$hostcolor$hbit\[\e[m\]$sepcolor:\[\e[m\][$dircolor\$(short_pwd)\[\e[m\]] $promptcolor$ebit\[\e[m\] "
#        fi
#
#fi
