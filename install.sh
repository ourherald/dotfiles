#!/usr/bin/env bash

# Script for installing dotfiles from this repo

generic_files=(".profile" ".tmux.conf" ".vimrc")

datetime=$(date +%Y%m%d-%H%M%S)
base=$(cd "$(dirname $0)" || exit ; pwd -P)
myshell=$(basename "$SHELL")

source "$base"/env_vars

# Debug info
echo "Running installer from $base"
echo "Operating system is $ostype"
echo "My user's shell is $myshell"
echo "XDG_CONFIG_HOME is $XDG_CONFIG_HOME"
echo "Config dir is $dot_conf_dir"


# Move dotfiles into place
install_dots() {
    if [[ ! -d "$dot_conf_dir" ]]; then
        mkdir -p "$dot_conf_dir"
    fi
    mv "$base" "$dot_dir"
}

## Move original dotfiles to archive
archive_old_dots() {
    # This outputs the filename including the path
    archive_dots="$dot_conf_dir"/old_dotfiles
    for item in "${generic_files[@]}"; do
	if [[ -L "$HOME/$item" ]]; then
	    unlink "$HOME/$item"
        elif [[ -f "$HOME/$item" ]]; then
            if [[ ! -d "$archive_dots" ]]; then
                mkdir -p "$archive_dots"
            fi
            ## Deal with exiting dotfiles
            mv "$HOME/$item" "$archive_dots"/"$item-$datetime"
            echo "Moved "$item to "$archive_dots/$item-$datetime"
        fi
    done
    if [[ -L "$HOME/.${myshell}rc" ]]; then
        unlink "$HOME/.${myshell}rc"
    elif [[ -f "$HOME/.${myshell}rc" ]]; then
        if [[ ! -d "$archive_dots" ]]; then
            mkdir -p "$archive_dots"
        fi
        mv "$HOME/.${myshell}rc" "$archive_dots/${myshellrc}-${datetime}"
    fi
}

## Make symlink to new files
symlink_dots() {
    for item in "${generic_files[@]}"; do
        ln -s "$dot_dir"/"$item" "$HOME"/
    done
}


archive_old_dots
install_dots
symlink_dots

source "$dot_dir"/functions/buildrc
buildrc

# Set relevant directories
if [[ $ostype == mac ]]; then
    XDG_CONFIG_HOME="$HOME"/Library/Preferences
else
    XDG_CONFIG_HOME="$HOME"/.config
fi

export dot_dot_conf_dir="$XDG_CONFIG_HOME"/dotfiles
