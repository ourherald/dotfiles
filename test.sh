#!/usr/bin/env bash

# Testing getting current working directory

#if [[ $(dirname "${BASH_SOURCE[0]}" | grep -c ".") == 1 ]]; then
#    echo $(dirname "${BASH_SOURCE[0]}")
#    echo $(dirname "${BASH_SOURCE[0]}" | tr './' "$(pwd)/")
#    base1=$(dirname "${BASH_SOURCE[0]}" | tr './' "$(pwd)/")
#    if [[ $($base | grep -c $HOME) == 1 ]]; then
#        base=$base1
#    fi
#else
#    base=$(dirname "${BASH_SOURCE[0]}")
#fi
#
#noDot=$base/dotfiles
#oldDir=$base/.oldDotfiles
#withDot=$base/.dotfiles

echo base       ... $base
echo noDot      ... $noDot
echo oldDir     ... $oldDir
echo withDot    ... $withDot

echo =======
echo "PWD variable = $(echo $PWD)"
echo "pwd function = $(pwd)"

echo "${BASH_SOURCE[0]}"
readlink ${BASH_SOURCE[0]}
dirname $(readlink ${BASH_SOURCE[0]})

scriptpath=$( cd "$(dirname $0)"; pwd -P )
outterbase=$(dirname $scriptpath)

echo "scriptpath = $scriptpath"
echo "outterbase = $outterbase"

#sourcedir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd) )"
#sourcedir2="$(dirname "${BASH_SOURCE[0]}")"
#echo "1 is $(readlink -f "${BASH_SOURCE[0]}")"
#echo "2 = $sourcedir2"
#echo "3 is $(find ${BASH_SOURCE[0]} -exec dirname {} \;)"
#echo "4 is "

#echo "$(dirname $sourcedir)"

## IP Functions
#mylanips() {
#    if [[ "$ostype" == linux ]]; then
#        ifaces=($(ifconfig | sed 's/[ \t].*//;/^$/d' | cut -d: -f1 | tr '\n' ' '))
#    else
#        ifaces=($(ifconfig -l))
#    fi
#
#    if [[ $(ifconfig | grep -c 'inet addr:') -eq 0 ]]; then
#        ifsyntax='inet '
#        ifsep=' '
#    else
#        ifsyntax='inet addr:'
#        ifsep=':'
#    fi
#    i=0
##    padlimit=60
##    pad=$(printf '%*s' "$padlimit")
#    #pad=${pad// /-}
#
#    # Trying to get padding characters into function output
#    while [[ $i -lt ${#ifaces[*]} ]]; do
#            #printf '%s%*.*s%s\n' "${ifaces[$i]}" 0 $((padlength - ${#string1} - ${#string2} )) "$pad" "$(ifconfig "${ifaces[$i]}" | grep "$ifsyntax" | awk '{print $2}')"
#            printf "\t%-14s-> %-1s %-1s\n" "${ifaces[$i]}" "$(ifconfig "${ifaces[$i]}" | grep "$ifsyntax" | awk '{print $2}')"
#        ((i++))
#    done
#}
#
#mywanip() {
#    if [[ ! -f $(command -v curl) ]]; then
#        printf '\t%-9s %s\n' "WAN" "$(wget -qO- https://icanhazip.com)"
#    else
#	    printf '\t%-9s %s\n' "WAN" "$(curl -sL https://icanhazip.com)"
#    fi
#}
#
#myip() {
#	mywanip
#	mylanips
#}
#
#myip
